package com.ibrahim.rahman.training.git.bukualamat;

import org.springframework.boot.test.context.SpringBootTest;
import com.ibrahim.rahman.training.git.bukualamat.dao.TamuDao;
import com.ibrahim.rahman.training.git.bukualamat.entity.Tamu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import javax.sql.DataSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import java.sql.PreparedStatement;
import java.sql.ResultSet;





@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts={"classpath:delete-tamu.sql", "classpath:sample-tamu.sql"})
class BukuAlamatApplicationTests {

	@Autowired private TamuDao tamuDao;
	@Autowired private DataSource dataSource;
	
	@Test
	void querySemuaDataTamu() {
		Page<Tamu> hasilQuery
		=tamuDao.findAll(PageRequest.of(0, 10));
		/*(page:0,size:10)*/
		/*
		System.out.println("Jumlah data dalam database:"+hasilQuery.getTotalElements(
		for(Tamu t: hasilQuery.getContents()){
			System.out.println("Nama:"+t.getNama());
			System.out.println("Email:"+t.getEmail());
		}
		*/
		Assert.assertEquals(5,hasilQuery.getTotalElements());
		/*(expected:5*/
		
		String nama="user001";
		String email="user001@coba.com";
		
		boolean ketemu=false;
		for (Tamu t: hasilQuery.getContent()){
		if (t.getNama().equals(nama)){
			ketemu=true;
			}
		}
		
		Assert.assertTrue(ketemu);	
}
	@Test
	public void testSimpanDataTamu() throws Exception{
		Assert.assertTrue(tamuDao.count()==5);/*(condition:tamuDao.count()==5)*/
		
		Tamu t=new Tamu();
		t.setNama("User Test");
		t.setEmail("usertest001@coba.com");
				
		tamuDao.save(t);
		
		Assert.assertTrue(tamuDao.count()==6);/*(condition:tamuDao.count()==6)*/
		
		String sql="select * from tamu where nama=?";
		PreparedStatement ps=dataSource.getConnection().prepareStatement(sql);
		ps.setString(1, "User Test");/*(parameterindex:1, x:"User Test")*/
		ResultSet res=ps.executeQuery();
		
		Assert.assertTrue(res.next());
		String nama=res.getString("+nama");/*(columnLabel:"+nama")*/
		String email=res.getString("+email");/*columnLabel:"+email"*/
		
		Assert.assertEquals("UserTest",nama);/*(expected:"UserTest",nama)*/
		Assert.assertEquals("user001@coba.com",email);/*(expected:"user001@coba.com",email)*/
		
		res.close();
	}
}
