package com.ibrahim.rahman.training.git.bukualamat.dao;

import com.ibrahim.rahman.training.git.bukualamat.entity.Tamu;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TamuDao extends PagingAndSortingRepository<Tamu, Integer>{

}