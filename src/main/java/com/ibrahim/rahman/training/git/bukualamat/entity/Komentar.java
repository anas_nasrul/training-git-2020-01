package com.ibrahim.rahman.training.git.bukualamat.entity;

import lombok.Data;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity @Data
public class Komentar {
    @Id @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name="id_tamu")
    private Tamu tamu;

    private LocalDateTime waktu;

    private String pesan;
}